Bluebox Esquire
===============

This is a reimplementation in KiCad of Don Froula's 
(http://projectmf.org/) PIC-based bluebox.  The circuit was by Don 
Froula and the board layout was by Phil Lapsley 
(http://explodingthephone.com).  It is so named because of Don's 
production of a close replica to the bluebox pictured in the October 
1971 Esquire article "Secrets of the Little Blue Box".  See 
https://www.youtube.com/watch?v=Kow8_N_dNts to see it in action.

There are three branches in this repository.  Branch 'v1' is as close a 
duplicate of the original board as I can manage.  As is, this board 
forms its own lid for the Radio Shack 230-1801 enclosure.  The 'v2' 
branch is modified such that it can fit in the bottom of the Radio Shack 
enclosure.  That one is probably a better choice for replicating Don's 
replica.  The master branch has been modified to fit a Hammond 1591XXM 
(dimensions 3.3" x 2.2" or 85mm x 56mm) enclosure, which I feel is of 
much better quality and utility.

This board requires six volts DC.  Two or four CR2032 coin cells can be 
mounted in onboard holders or six volts applied to an external power 
header.  Keystone 103 holds one cell each.  Keystone 1026 and MPD BH800S 
hold two cells each stacked.  I chose to try the MPD BH800S because I 
was uncertain if the Keystone 1026 would fit within the confines of the 
case.

See project homepage at https://661.org/proj/bluebox for complete 
documentation and build instructions.

The firmware for this project is found at 
https://gitlab.com/DavidGriffith/bluebox-avr/



Bill Of Materials
-----------------

**Onboard parts**

| Location             | Description                 | Value                |
|----------------------|-----------------------------|----------------------|
| R1 - R14             | 1/4W resistor               | 1K                   |
| R15                  | 1/4W resistor               | 100K                 |
| R16, R17             | 1/4W resistor               | 150K                 |
| R18, R19             | 1/4W resistor               | Depends on LED choice|
|                      |                             |                      |
| C1                   | radial elec or tantalm cap  | 22uF                 |
| C2                   | ceramic capacitor           | 0.1uF                |
| C3, C4               | ceramic capacitor           | Depends on crystal   |
| C5, C6, C7           | ceramic capacitor           | 0.33uF               |
|                      |                             |                      |
| X1                   | HC49-U low-profile xtal     | 20Mhz                |
|                      |                             |                      |
| D1 [^3]              | DO-41 rectifier diode       | 1N4005               |
| IC1                  | DIP8 AVR microcontroller    | ATtiny85             |
| LED1, LED2           | 1.8mm LED                   |                      |
|                      |                             |                      |
| B1, B2               | CR2032 cell holders         | Keystone 103, 1026,  |
|                      |                             |   or MPD BH800S      |
| CONN1 - CONN3        | 2x1 0.1-inch pitch          |                      |
|                      |   right angle header        |                      |
| CONN4                | 2x3 0.1-inch pitch          |                      |
|                      |   vertical header           |                      |
| SW1 - SW13           | 6mm tactile switch          | TE/Alco FSM11JH      |
|                      |   4.4mm stem height         |                      |

**Other Parts**

| Designation        | Description                    | Notes              |
|--------------------|--------------------------------|--------------------|
| Enclosure          | Hammond 1592 XXM               |                    |
| AAA Battery Holder | Keystone 2482 or equivalent    | Optional           |
| Power Switch       | Miniature or sub-mini          | E-Switch EG1201    |
|                    |   SPST switch                  |   or similar       |
| Audio Jack         | 3.5mm switched mono phone jack | Switchcraft 35PM2A |
|                    |                                |   or similar       |
| External Speaker   | "500" type handset speaker     | NS-E004, HHMR-80   |
|                    |                                |   or equivalent    |
